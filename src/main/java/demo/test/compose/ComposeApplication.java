package demo.test.compose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "demo.test.compose.model")
public class ComposeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComposeApplication.class, args);
		System.out.println("Testing");
	}

}
