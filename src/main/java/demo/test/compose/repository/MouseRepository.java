package demo.test.compose.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.test.compose.model.Mouse;

public interface MouseRepository extends JpaRepository<Mouse, Integer> {

}
