package demo.test.compose.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.test.compose.model.Mouse;
import demo.test.compose.repository.MouseRepository;

@RestController
public class TestController {
    
    @Autowired
	MouseRepository mouseRepository;
	
	@GetMapping("/hello")
	public String helloWorld() {
		return "Hai from hello API";
	}
		
	@GetMapping("/mouse")
	public List<Mouse> hello() {
		System.out.println("Hai from status type API");
		List<Mouse> mouses = mouseRepository.findAll();
		System.out.println(mouses);
		return mouses;	
	}
	
	

}
