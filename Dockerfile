# Use a Maven image as a builder stage
FROM maven:3.8.4-openjdk-17-slim AS builder

# Set the working directory in the container
WORKDIR /app

# Copy the Maven configuration file copy
COPY pom.xml .

# Download dependencies and plugins
RUN mvn dependency:go-offline

# Copy the source code into the container
COPY src ./src

# Build the application
RUN mvn package -DskipTests

# Use a base image with Java 17 pre-installed
FROM openjdk:17-slim

# Set the working directory in the container
WORKDIR /app

# Copy the packaged JAR file from the builder stage to the current directory in the container
COPY --from=builder /app/target/compose-0.0.1-SNAPSHOT.jar /app/compose-0.0.1-SNAPSHOT.jar

# Command to run the Spring Boot application
CMD ["java", "-jar", "compose-0.0.1-SNAPSHOT.jar"]
